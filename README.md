# SystemC Photonic NoC

The simulations are based off of NoCSim. NoC Sim is a simulator that simulates the Electrical Network on Chip. NoCSim provides the following features that proves helpful in using the code for Photonic NoCs


- Different traffic patterns
- Different Routing methodologies


The following Modifications have been done to the code that provides support for Photonic NoC's

- Addition of channels to simulate the the DWDM in Photonic 
- Circular Buffer for token ring in models such as flexishare, ultranoc etc.
- Modification of the Hub to enable the different architectures
- Addition of timing based traffic injection to perform benchmark testing
- Modification of Statistics collection module to get the timing analysis


## Architecture

the order of hierarchy is as follows


### TNoC:
Top Module that contains all the Modules

Does the following functionality:

- Creates GITiles, which are the NoC endpoints for each core
- Creates a global arbitrator which creates traffic in the NoC
- Builds the connections in the PNoC i.e. each Tile's io ports are connected to the arbiter

### Arbiter:

This is the module that generates the traffic in the network. 

- The arbiter connects to all the tiles(cores)  
- Generates traffic based on the times given in the benchmark.
- Receives and updates the response for the traffic generated


### Benchmark

The way in which the benchmarks are fed to the PNoC systemC simulator is by providing time traces. An example of a time trace is:

```
10000 02 MemRead
10000.8 02 MemWrite
10001.0 02 MemRead
.
.
.

```

The time and source address at which the traffic is generated is provided as inputs to the benchmark module. This module reads and generates the traffic at the appropriate instance.

The traces are generated using the gem5 simulator.


### Tile

The tiles are the endpoint/generator of the traffic. Each tile has the following modules in them:

- Gateway Interface
- Hub
- Processing Element
- stats

Each tile is connected to the arbiter for generating the traffic in the current model.


### Gateway interface

The module gets each packet from the arbiterator and logs the received packets. 

- It counts the delays incurred in receiving the packets(flits). 
- Since the data is read by the micro rings and written to a buffer, it checks if the buffer is full updates the data accordingly.
- Takes into consideration the MWMR and forwards the data to the next destination accordingly.

### HUB

Hub emulates the 4 hub split in the flexishare architecture, and forwards the data to the corresponding Processing element. While doing so, also updates the power due to arbiteration, crossbar losses, link losses, routing loss, buffering and leakage.


### Processing element

Used in Electrical NoC to check for received packet and update the stats accordingly and generates the acknowledgement. Also implements route forwarding in case of different routing schemes.

### Stats

The stats module has the following statistics
- Delay/Latency
- Energy consumed
- Power
- Packets tx/rx stats and injection ratio


## Modification Instructions

As explained above, all the communication happens at the gateway interface, hub and the processing element. Modify the architecture based on the connections of the three modules, how the rx and tx process interact, and update the stats accordingly. If needed, add additional functions to incorporate the different losses and delays in the code.

To verify that the code is working as expected, you will need to 

if you need and example, compare the master branch with the swiftNoC branch as below

``` bash
	git diff -w --ignore-space-at-eol ultra_improved .
```

## Code Compilation instructions:

The compilation is tested in Ubuntu 16.04+ and OSX. the compilation will not work on windows without modification of CMakeLists.txt

Download the repository using:

``` bash
git clone https://varunbhat@bitbucket.org/varunbhat/photonic-noc-systemc.git
```

The respoitory has different branches for different architectures to list the repository, execute:
```
git branch --remote
# checkout to the required architecture
git checkout flexishare2preal256
```

**SystemC Installation:**

The script to install systemc can be found at *bin/install_systemc*

on the terminal cd into the project directory(where CMakeLists.txt exists) and execute

```
./bin/install_systemc
```

This should install the required packages and link the libraries properly as used in the CMakeLists.txt

**Code Compilation**

Requirements:
	* cmake

install cmake for ubuntu using:

```
sudo apt-get install build-essential cmake
```

Compile the code usind the following commands:
```
#in the directory where CMakeLists.txt exists, execute the command
cmake .
make
```

This should compile the source and create an executible in bin directory

cd into *etc* directory and execute the script
```
bash ../bin/scriptnew.sh
```

The executable takes in 4 traceinput files as arguments and prints the simulation results on the STDOUT. The scriptnew.sh redirects the output to a file. you can modify the script to your needs.
The trace files are present in *etc* directory.

## Additional Instructions

The code is compatible with **SystemC-2.3.0** only. Any version above will not compile.
You will need to add the following export statement in your commandline. so that you do not get the multi write error.

``` bash
export SC_SIGNAL_WRITE_CHECK=DISABLE
```

The CMakeLists.txt sets up the correct version of the compiler.If you need to compile the code without cmake, use SystemC with gcc-5 and above. and this PNoC code with gcc5 with *-std=98*


Running the code directly results in a seg fault. Run the code with the benchmark files. The shell scripts in the bin folder runs the complete test cases with the proper benchmarks. If required, an additional patch has been added in the flexishare2preal256 branch in benchmark.c/.h files.

**Happy coding**
